<?php

/**
 * @file
 * Admin page callbacks for the alternative_block_ui module.
 */

/**
 * Form builder; Configure alternative_block_ui settings.
 *
 * @ingroup forms
 * @see system_settings_form()
 */
function alternative_block_ui_admin_settings() {
  $form['alternative_block_ui_blocks_list'] = array(
    '#title' => t('Enable blocks'),
    '#description' => t('Unchecked blocks will not be used in alternative layout.'),
    '#type' => 'checkboxes',
    '#required' => TRUE,
    '#options' => _alternative_block_ui_get_blocks(),
    '#default_value' => variable_get('alternative_block_ui_blocks_list', array(0 =>1)),
  );
  return system_settings_form($form);
}

/**
 * Helper function, get blocks
 * @return array of system blocks
 */
function _alternative_block_ui_get_blocks() {
  $query = db_select('block', 'b');
  $query->groupBy('b.delta');
  $query->fields('b',array('module','delta'));
  $result = $query->execute();
  $block_options = array();
  while($record = $result->fetchAssoc()) {
    $block_id = "{$record['module']}_{$record['delta']}";
    $block_machine_id = "{$record['module']}-*-{$record['delta']}";
    $block_options[$block_machine_id] = $block_id;
  }
  return $block_options;
}